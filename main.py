#!/usr/bin/env python3
import json, sys

class Chargen:
  def __init__(self, loadfile="basic.json"):
    jdec = json.JSONDecoder()
    try:
      loadfile = open(loadfile).read()
    except:
      print("Bad file given.")
      sys.exit()
    temp = jdec.decode(loadfile)
    global prompts, info
    prompts = temp["main"]
    info = temp["info"]
  
  def main(self):
    file_o = input("Output file: ")
    try:
      file_o = open(file_o, "w+")
    except:
      print("Bad input given.")
      sys.exit()
    print("\n" + info["header"] + " by " + info["author"], info["description"], sep="\n")
    output_f = ""
    for i in prompts:
      output_i = ""
      if "section" in i.keys():
        print("\n" + i["section"], sep="")
        output_i = "\n" + i["section"] + "\n"
      j = input(i["prompt"] + " ")
      output_i = output_i + i["prompt"] + " " + j + "\n"
      output_f += output_i
    if output_f[0] == "\n":
      output_f = output_f[1:]
    file_o.write(output_f)
    file_o.close()

cg = Chargen()
try:
  cg.main()
except KeyboardInterrupt:
  print("Interuppted.")
