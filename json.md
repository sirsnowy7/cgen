# cgen JSON
A cgen JSON file is split into two parts: `info` and `main`
You can easily fill these out yourself. If you do, also, you can fork this repository with your file in the "generators" folder, and I'll validate the code and merge it with the master repo.

## info
The info section has 3 currently used properties. A header, which is basically a title, an author, which is to be filled with the author's username, and a description, which is however you choose to describe yours, e.g. "Character sheet generator for D&D 5th Edition characters."

## main
The main section is for every prompt and question you mean to ask. Each entry is stored in an array, which would be `main: []`. They each have up to two active properties, which are `prompt` and `section`. Every prompt with a section property will have the section content before it, e.g. "stats". Each prompt will be printed, then recieve input from the user, which is wrote to the file.
